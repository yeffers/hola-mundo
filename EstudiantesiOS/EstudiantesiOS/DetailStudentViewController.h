//
//  DetailStudentViewController.h
//  EstudiantesiOS
//
//  Created by centro docente de computos on 11/03/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Estudiantes.h"

@interface DetailStudentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *addreslabel;
@property (weak, nonatomic) IBOutlet UILabel *phonenumberlabel;

-(instancetype)initWithStudent:(Estudiantes *)student;

@end
