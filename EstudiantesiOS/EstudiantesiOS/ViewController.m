//
//  ViewController.m
//  EstudiantesiOS
//
//  Created by centro docente de computos on 6/03/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "ViewController.h"
#import "DetailStudentViewController.h"
#import "Estudiantes.h"

@interface ViewController ()

@end

@implementation ViewController {NSArray *students;}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"Estudiantes Udem";
    Estudiantes *student = [[Estudiantes alloc]init];
    students = [student studentUdeM];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return students.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell= [self.mainTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
       cell = [[UITableViewCell alloc]
        initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Estudiantes *dataStudent= students[indexPath.row];
    
    cell.textLabel.text = dataStudent.name ;
                                                             
    return cell;
}





#pragma mark - UITableViewDelegate



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UIAlertView *alert= [[UIAlertView alloc]initWithTitle:@"cell" message:[NSString stringWithFormat:@"Estoy en la cell %li", indexPath.row ] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
   [alert show];
    
    DetailStudentViewController *studentVC = [[DetailStudentViewController alloc]initWithStudent:students[indexPath.row]];

    
    [self.navigationController pushViewController:studentVC animated:YES];

}









@end
