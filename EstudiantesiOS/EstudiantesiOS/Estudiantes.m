//
//  Estudiantes.m
//  EstudiantesiOS
//
//  Created by centro docente de computos on 11/03/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "Estudiantes.h"



@implementation Estudiantes

- (instancetype)initWithIdStudent:(NSInteger)iDStudent name: (NSString *) name address:(NSString *)address phoneNumber:(NSString *)phoneNumber{
    
    if (self = [super init]){
        self.iDStudent = iDStudent;
        self.name = name;
        self.address = address;
        self.phoneNumber = phoneNumber;
    }
    
    return self;
}

-(NSArray *)studentUdeM{
    NSMutableArray *students = [NSMutableArray array];
    
    Estudiantes *student = [[Estudiantes alloc]initWithIdStudent:1 name:@"NN" address:@"Calle 1" phoneNumber:@"1234"];
    [students addObject:student];
    
    Estudiantes *student2 = [[Estudiantes alloc]initWithIdStudent:2 name:@"MM" address:@"Calle 2" phoneNumber:@"1235"];
    [students addObject:student2];
    
    Estudiantes *student3 = [[Estudiantes alloc]initWithIdStudent:3 name:@"OO" address:@"Calle 3" phoneNumber:@"1236"];
    [students addObject:student3];
    
    return students.copy;
}

@end
