//
//  DetailStudentViewController.m
//  EstudiantesiOS
//
//  Created by centro docente de computos on 11/03/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "DetailStudentViewController.h"

@interface DetailStudentViewController ()

@end

@implementation DetailStudentViewController{Estudiantes *dataStudent;}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.addreslabel.text= dataStudent.address;
    self.phonenumberlabel.text = dataStudent.phoneNumber;
    
}

-(instancetype)initWithStudent:(Estudiantes *)student{
    if (self = [super init]) {

        self.title = student.name;
        dataStudent = student;
   
    }
    return self;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
