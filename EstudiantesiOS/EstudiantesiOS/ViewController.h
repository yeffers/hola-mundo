//
//  ViewController.h
//  EstudiantesiOS
//
//  Created by centro docente de computos on 6/03/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end

