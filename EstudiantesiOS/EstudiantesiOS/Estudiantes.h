//
//  Estudiantes.h
//  EstudiantesiOS
//
//  Created by centro docente de computos on 11/03/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Estudiantes : NSObject

@property(assign, nonatomic) NSInteger iDStudent;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *phoneNumber;

//designado Inicie con el id student el nombre adress y phone number
- (instancetype)initWithIdStudent:(NSInteger)iDStudent name: (NSString *) name address:(NSString *)address phoneNumber:(NSString *)phoneNumber;

-(NSArray *)studentUdeM;
@end
